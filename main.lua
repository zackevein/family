local ni = ...
if not asdasd then asdasd = false end

local lhw = GetSpellInfo(49276)
local hw = GetSpellInfo(49273)
local ttt = 0

local function myasd()
    if not asdasd then return end

    -- change it between 0.5 - 0.2
    if not defined then defined = true
        local n = UnitName("player")
        print("[Shaman] Welcome back, lord "..n..". Kill them all. No Mercy.")

        -- last noticed
        last = nil
        -- timeout noticed
        noticed = 0
        stopc = 0
        timerc = 0

        spells = {
            [6215] = 3,		-- Peur
            [6358] = 3,		-- Séduction
            [17928] = 4,	-- Hurlement de terreur
            [12826] = 3,	-- Polymorph
            [12824] = 3,	-- Polymorph
            [12825] = 3,	-- Polymorph
            [118] = 3,		-- Polymorph
            [61305] = 3,	-- Polymorph: Black Cat
            [28272] = 3,	-- Polymorph: Pig
            [61721] = 3,	-- Polymorph: Rabbit
            [61780] = 3,	-- Polymorph: Turkey
            [28271] = 3,	-- Polymorph: Turtle
            [33786] = 3,	-- Cyclone
            [18656] = 3,	-- Hibernation
            [12051] = 3,	-- Evocation
            [691] = 4,	-- petlock
            [712] = 4,	-- petlock
            [48071] = 2,
            [48063] = 2,
            [48120] = 2,
            [48785] = 2,
            [48782] = 2,
            [50464] = 2,
            [48443] = 2,
            [48378] = 2,
            [49273] = 2,
            [49276] = 2,
            [49271] = 2,
            [47811] = 2,
            [52985] = 2,
            [47750] = 2,
            [53007] = 2,
            [41846] = 2,
            [64901] = 2,
            [64843] = 2,
            [60043] = 3,
            [50796] = 3,
            [51514] = 1,
            [48160] = 3,
            [605] = 3,
            [8129] = 3,
            [48160] = 3,
            [47843] = 3
        }

        holder = {}
        for u,v in pairs(spells) do
            holder[select(1, GetSpellInfo(u))] = v
        end

        party = {"party1","party2", "party1pet", "party2pet", "player"}

        handled = {
            [2094] = "blind",
            [1776] = "gouge",
            [20066] = "repentance",
            [19503] = "scatter"
        }

        ArenaEnemies = {
            "arenapet1",
            "arenapet2",
            "arenapet3",
            "arena1",
            "arena2",
            "arena3",
            "arena1pet",
            "arena2pet",
            "arena3pet"
        }


        Enemies = {
            "target",
            "targettarget",
            "focustarget",
            "focus",
            "mouseover"
        }

        swapped = nil
        function CdRemains(spellId, gcd)
            if gcd == nil then gcd = true end
            local duration = select(2, GetSpellCooldown(spellId))

            if gcd then
                return not (duration
                        + (select(1, GetSpellCooldown(spellId)) - GetTime()) >= 0)
            else
                return 2 - duration >= 0
            end
        end

        function castAoe(spellid)
            ni.spell.cast(spellid)
            CameraOrSelectOrMoveStart()
            CameraOrSelectOrMoveStop()
        end

        function HealthIsUnder(unit, percent)
            return (((100 * UnitHealth(unit) / UnitHealthMax(unit))) < percent)
        end

        function IsArena()
            return select(1, IsActiveBattlefieldArena()) == 1
        end

        function GetEnemies()
            if select(1, IsActiveBattlefieldArena()) == 1 then
                return ArenaEnemies
            else
                return Enemies
            end
        end

        function remains(spellid)
            if select(2, GetSpellCooldown(spellid))
                    + (select(1, GetSpellCooldown(spellid)) - GetTime()) > 0 then
                return 0
            else return 1
            end
        end

        function ValidUnitType(unitType, unit)
            local isEnemyUnit = UnitCanAttack("player", unit) == 1
            return (isEnemyUnit and unitType == "enemy")
                    or (not isEnemyUnit and unitType == "friend")
        end

        function ValidUnit(unit, unitType)
            return UnitExists(unit)==1 and ValidUnitType(unitType, unit)
        end

        function notice(message, party)
            if party == nil then party = false end

            if message ~= last or (GetTime() - timeout) > 2 then
                PlaySound("RaidWarning", "master")

                if party then
                    SendChatMessage(message,"PARTY", GetDefaultLanguage("player"), 1)
                else
                    RaidNotice_AddMessage(RaidWarningFrame, message, ChatTypeInfo["RAID_WARNING"])
                end

                last = message
                timeout = GetTime()
            end
        end

        function IsTargetingParty(unit)
            for _, member in ipairs(party) do
                if UnitExists(member) then
                    if UnitName(member) == tartar then
                        return true
                    end
                end
            end

            return false
        end

        function CanCast(id, unit)
            unit = unit or ""
            return CdRemains(id, false)
                    and (unit == "" or IsSpellInRange(GetSpellInfo(id), unit) == 1)
        end

        function CastSpellA(id, unit)
            unit = unit or ""
            if CdRemains(id, true)
                    and (unit == "" or (IsSpellInRange(GetSpellInfo(id), unit) == 1 and ni.unit.los("player", unit))) then
                ni.spell.cast(id, unit)
            end
        end

        local _UnitBuff = UnitBuff
        local _UnitDebuff = UnitDebuff
        local _UnitAura = UnitAura


        local function UnitBuff(unit, buff)
            if unit == nil or buff == nil then return end
            for i=1,40 do
                local name = _UnitBuff(unit, i)

                if not name then break end

                if name == buff then
                    return _UnitBuff(unit, i)
                end
            end
            return nil
        end

        local function UnitDebuff(unit, buff)
            if unit == nil or buff == nil then return end
            for i=1,40 do
                local name = _UnitDebuff(unit, i)

                if not name then break end

                if name == buff then
                    return _UnitDebuff(unit, i)
                end
            end
            return nil
        end

        local function UnitAura(unit, buff)
            if unit == nil or buff == nil then return end
            for i=1,40 do
                local name = _UnitAura(unit, i)

                if not name then break end

                if name == buff then
                    return _UnitAura(unit, i)
                end
            end
            return nil
        end

        local function UnitBuffID(unit, id)return UnitBuff(unit, GetSpellInfo(id)) end
        local function UnitDebuffID(unit, id)return UniDebuff(unit, GetSpellInfo(id)) end
        local function UnitAuraID(unit, id)return UnitAura(unit, GetSpellInfo(id)) end

        function HasAura(id, unit)
            return UnitBuffID(unit, id) ~= nil
                    or UnitDebuffID(unit, id) ~= nil
        end

        function HasAura(id, unit)
            return UnitDebuff(unit, GetSpellInfo(id)) ~= nil or UnitBuff(unit, GetSpellInfo(id)) ~= nil or
                    select(11, UnitAura(unit, GetSpellInfo(id))) == id
        end

        function TremorUp()
            local a, name, _, duration = GetTotemInfo(2)

            return a ~= nil and duration ~= nil and duration > 0 and name == "Totem de séisme"
        end

        function FireUp()
            local a, name, _, duration = GetTotemInfo(1)

            return a ~= nil and duration ~= nil and duration > 0
        end

        function GroundUp()
            local a, name, _, duration = GetTotemInfo(4)

            return a ~= nil and duration ~= nil and duration > 0 and name == "Totem de glèbe"
        end

        function EnnemiesAreUnder(percent)
            for _, enmy in ipairs(GetEnemies()) do
                if ValidUnit(enmy, "enemy") then
                    if HealthIsUnder(enmy, 80)	then
                        return true
                    end
                end
            end

            return false
        end
    end

    -- listening spell events
    if not singleton then
        singleton = true



        local interruptID = {
            [1766] = true, 		--Kick
            [36554] = true, 	--Spell Lock
            [2139] = true, 		--Counterspell
            [47476] = true,		--Strangulate
            [47528] = true, 	--Mind Freeze
            [57994] = true, 	--Wind Shear
            [6552] = true, 		--Pummel
            [72] = true, 		--Shield Bash
            [31935] = true,		--Avenger&amp;apos;s Shield
            [34490] = true, 	--Silencing Shot
        }


        flood = 0
        ftime = 0

        local SIN_PlayerGUID = UnitGUID("player")
        local SIN_InterruptFrame = CreateFrame("FRAME", nil, UIParent)
        SIN_InterruptFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
        SIN_InterruptFrame:RegisterEvent("UI_ERROR_MESSAGE")
        SIN_InterruptFrame:SetScript("OnEvent",
                function(self, event, arg1, type,  sourceGUID, sourceNAME, _, destGUID, destNAME, _, spellID )
                    if spellID == 10890 then
                        local old = nil
                        if UnitExists("target") then
                            old = UnitName("target")
                        end
                        local e = select(1, GetSpellInfo(529))

                        RunMacroText("/target "..sourceNAME)
                        if not TremorUp()
                                and ValidUnit("target", "enemy")
                                and ni.unit.distance("player", "target") <= 30 then
                            ni.spell.cast(8143)
                        end

                        if old ~= nil then
                            RunMacroText("/target "..old)
                        else
                            ClearTarget()
                        end
                    elseif type == "SPELL_CAST_SUCCESS" and destGUID == SIN_PlayerGUID and interruptID[spellID] then
                        local isProtected =  select(9, UnitCastingInfo("player")) or select(8, UnitChannelInfo("player"))
                        if not isProtected then
                            SpellStopCasting()
                            SpellStopCasting()
                            SpellStopCasting()
                        end
                    elseif event == "UI_ERROR_MESSAGE" and (arg1:find("Rien à dissiper")or arg1:find("Nothing to dispel")) then
                        flood = 2
                    elseif (type == "SPELL_DAMAGE" or type == "SWING_DAMAGE" or type == "RANGE_DAMAGE") and destNAME == "Totem de séisme" then
                        local old = nil
                        if UnitExists("target") then
                            old = UnitName("target")
                        end

                        RunMacroText("/target "..sourceNAME)

                        if ValidUnit("target", "enemy") then
                            PlaySoundFile("Interface\\AddOns\\PowerAuras\\Sounds\\swordecho.wav")
                            notice("TREMOR BROKEN")
                        end

                        if old ~= nil then
                            RunMacroText("/target "..old)
                        else
                            ClearTarget()
                        end
                    end
                end)

        lags = 0.35
        if UnitName("player") == "Lockshamhero" then
            lags = 0.35
        end

        local function CastingInfo(u)
            local channel = false
            local name, _, _, _, startTimeMS, endTimeMS, _, _, notInterruptible = UnitCastingInfo(u)
            if not name then
                name, _, _, _, startTimeMS, endTimeMS, _, notInterruptible = UnitChannelInfo(u)
                channel = true
            end

            return name, nil, startTimeMS, endTimeMS, notInterruptible, channel
        end

        local function BuffInfo(spell, unit)
            if unit == nil then return end
            for i=1,40 do
                local name, _, _, _, duration, endTimestamp = UnitBuff(unit, i)

                if not name then break end

                if name == spell then
                    return duration, endTimestamp
                end
            end
            return nil, nil
        end

        local function InLos(unit, other)
            if unit == nil then return false end
            other = other or "player"

            return ni.unit.los(unit, other)
        end

        local shear = GetSpellInfo(57994)
        local function Cs(unit)
            if not UnitExists(unit) then return end
            if BuffInfo("Effet du Totem de glèbe", unit) then
                if InLos(unit) and GetSpellCooldown(shear) == 0 and IsSpellInRange(shear, unit) == 1 then
                    ni.spell.cast(shear, unit)
                    return
                end
                return
            end
            local casting,_,_,_,protected = CastingInfo(unit)
            if not casting or protected then return end
            if IsSpellInRange(shear, unit) == 1 then
                ni.spell.cast(shear, unit)
            end
        end
        _G.weakauras6 = Cs

        function _G.weakauraShow(unit)
            if HasAura(16188, "player") then
                if CdRemains(55198, false) then
                    CastSpellA(55198)
                end

                CastSpellA(49273, unit)
            else
                local lesser = true

                if HasAura(12294, unit)
                        or HasAura(19434, unit)
                        or HasAura(57975, unit) then
                    lesser = not HealthIsUnder(unit, 60)
                elseif HealthIsUnder(unit, 50) then
                    lesser = false
                end

                if lesser then
                    CastSpellA(49276, unit)
                else
                    if not HasAura(53390, "player") and CdRemains(59547) then
                        CastSpellA(59547, unit)
                    end
                    CastSpellA(49273, unit)
                end
            end
        end

        function weakauraHide()
            local t = "target"

            if CdRemains(60043, false) then
                CastSpellA(60043, t)
            elseif CdRemains(49271, false) then
                CastSpellA(49271, t)
            else
                CastSpellA(49238, t)
            end
        end

        -- 41% CC 20% non CC
        -- 		  50% non CC 53390
        -- Parse a recevied command (/lua are listened)
        function ParseCommand(command)
            if string.match(command, "/lua") == nil then return end
            local action = string.gsub(command, "/lua ", "")

            if string.find(action, "lags") ~= nil then
                if -lags > -0.5 then
                    lags = lags + 0.05
                else
                    lags = 0.25
                end
                print("New end ms: "..lags)
            elseif string.find(action, "heal") ~= nil then
                local unit = string.gsub(action, "heal ", "")

                if HasAura(16188, "player") then
                    if CdRemains(55198, false) then
                        CastSpellA(55198)
                    end

                    CastSpellA(49273, unit)
                elseif HasAura(53390, "player") or not CdRemains(61301, false) then
                    local lesser = true

                    if HasAura(12294, unit)
                            or HasAura(19434, unit)
                            or HasAura(57975, unit) then
                        lesser = not HealthIsUnder(unit, 60)
                    elseif HealthIsUnder(unit, 50) then
                        lesser = false
                    end

                    if CdRemains(61301, false) then
                        CastSpellA(61301, unit)
                    else
                        if lesser then
                            CastSpellA(49276, unit)
                        else
                            if not HasAura(53390, "player") and CdRemains(59547) then
                                CastSpellA(59547, unit)
                            end
                            CastSpellA(49273, unit)
                        end
                    end
                else
                    CastSpellA(61301, unit)
                end
            elseif string.find(action, "attack") ~= nil then
                local t = "target"

                if CdRemains(60043, false) then
                    CastSpellA(60043, t)
                elseif CdRemains(49271, false) then
                    CastSpellA(49271, t)
                else
                    CastSpellA(49238, t)
                end
            elseif string.find(action, "melee") ~= nil then
                local t = "target"

                _, _, _, count, _, _, time, _, _, _, _ = UnitBuff("player", GetSpellInfo(53817))
                if count == 5 then
                    CastSpellA(49238, t)
                end
                if CdRemains(17364, false) then
                    CastSpellA(17364, t)
                end
                if CdRemains(49231, false) then
                    CastSpellA(49231, t)
                end
                CastSpellA(60103, t)
            end
        end

        -- Checking macro commands
        local commandFrame = CreateFrame("FRAME", nil, UIParent)
        commandFrame:RegisterEvent("EXECUTE_CHAT_LINE")
        commandFrame:SetScript("OnEvent",
                function(self, event, ...)
                    ParseCommand(arg1)
                end
        )

        function catch(unit, ground)
            if ground == nil then ground = false end

            local currentCast = UnitCastingInfo("player")
            if currentCast == lhw or currentCast == hw then return end

            local distFromCaster = ni.unit.distance("player", unit)
            local distFromParty1 = ni.unit.distance("player", "party1")
            if ground and (CdRemains(8177, true)) and (distFromCaster <= 40 or distFromParty1 <= 30) then
                SpellStopCasting()
                CastSpellA(8177)
            elseif not GroundUp() and CdRemains(57994) and IsSpellInRange(GetSpellInfo(57994), unit) == 1 and ni.unit.los("player", unit) then
                if not HasAura(8178, "player") then
                    SpellStopCasting()
                    CastSpellA(57994, unit)
                end
            end
        end

        function catchGround(unit)
            local currentCast = UnitCastingInfo("player")
            if currentCast == lhw or currentCast == hw then return end

            local distFromCaster = ni.unit.distance("player", unit)
            local distFromParty1 = ni.unit.distance("player", "party1")

            if ground and (CdRemains(8177, true)) and (distFromCaster <= 40 or distFromParty1 <= 30) then
                SpellStopCasting()
                CastSpellA(8177)
            end
        end

        function HealthTeamNotUnder(percent)
            for _, member in ipairs(party) do
                if HealthIsUnder(member, percent) then
                    return false
                end
            end

            return true
        end

        function _G.weakra()
            if not FireUp() then
                CastSpellA(58734)
            elseif CdRemains(61657, true) then
                --SpellStopCasting()
                CastSpellA(61657)
            end
        end

        function noGroundAndReflect(unit)
            local reflect = HasAura(59725, unit) or HasAura(23920, unit)

            if HasAura(8178, unit) or reflect then
                if flood == 0 or (GetTime() - ftime > 0.5) then
                    ftime = GetTime()
                    flood = 1
                end


                local currentCast = UnitCastingInfo("player")
                if currentCast == lhw or currentCast == hw then return end

                if flood == 1 and not HasAura(32182, "player") and IsSpellInRange(GetSpellInfo(8012), unit) == 1 and ni.unit.los("player", unit) then
                    SpellStopCasting()
                    CastSpellA(8012, unit)
                elseif flood == 2 then
                    if CdRemains(57994, false) and reflect and ni.unit.los("player", unit) and IsSpellInRange(GetSpellInfo(57994), unit) == 1 then
                        SpellStopCasting()
                        CastSpellA(57994, unit)
                    elseif CdRemains(49233, false) and ni.unit.los("player", unit) and IsSpellInRange(GetSpellInfo(49233), unit) == 1 then
                        SpellStopCasting()
                        CastSpellA(49233, unit)
                    end
                end
            end
        end

        x1 = CreateFrame("Frame", nil, UIParent)
        x1:SetPoint("CENTER", -32, -130)
        x1:SetWidth(14)
        x1:SetHeight(14)
        t1 = x1:CreateTexture("ARTWORK")
        t1:SetAllPoints()
        t1:SetTexture(1.0, 0, 0)
        x1:Show()

        x2 = CreateFrame("Frame", nil, UIParent)
        x2:SetPoint("CENTER", -16, -130)
        x2:SetWidth(14)
        x2:SetHeight(14)
        t2 = x2:CreateTexture("ARTWORK")
        t2:SetAllPoints()
        t2:SetTexture(1.0, 0, 0)
        x2:Show()

        x3 = CreateFrame("Frame", nil, UIParent)
        x3:SetPoint("CENTER", 0, -130)
        x3:SetWidth(14)
        x3:SetHeight(14)
        t3 = x3:CreateTexture("ARTWORK")
        t3:SetAllPoints()
        t3:SetTexture(1.0, 0, 0)
        x3:Show()

        x4 = CreateFrame("Frame", nil, UIParent)
        x4:SetPoint("CENTER", 16, -130)
        x4:SetWidth(14)
        x4:SetHeight(14)
        t4 = x4:CreateTexture("ARTWORK")
        t4:SetAllPoints()
        t4:SetTexture(1.0, 0, 0)
        x4:Show()

        x5 = CreateFrame("Frame", nil, UIParent)
        x5:SetPoint("CENTER", 32, -130)
        x5:SetWidth(14)
        x5:SetHeight(14)
        t5 = x5:CreateTexture("ARTWORK")
        t5:SetAllPoints()
        t5:SetTexture(1.0, 0, 0)
        x5:Show()

        if GetSpellCooldown(GetSpellInfo(51533)) == nil then
            t1:Hide()
            t2:Hide()
            t3:Hide()
            t4:Hide()
            t5:Hide()
        end

        llastx = 0

        rebuff = 0
    end


    for _, member in ipairs(party) do
        if ValidUnit(member, "friend") then
            if (HasAura(10890, member)
                    or HasAura(6215, member)
                    or HasAura(6358, member)
                    or HasAura(5246, member)
                    or HasAura(20511, member)
                    or HasAura(17928, member))
            then
                if  not TremorUp() then
                    if ni.unit.distance("player", member) <= 30 then
                        CastSpellA(8143)
                    elseif GetTime() - ttt > 2 then
                        ni.player.runtext("/p WANT A TREMOR?")
                        PlaySound(10590)
                        ttt = GetTime()
                    end
                elseif GetTime() - ttt > 2 then
                    ni.player.runtext("/p TREMOR IS ALREADY UP")
                    PlaySound(10590)
                    ttt = GetTime()
                end

            elseif HasAura(51724, member) then
                if not FireUp() then
                    CastSpellA(58734)
                elseif CdRemains(61657, true) then
                    SpellStopCasting()
                    CastSpellA(61657)
                end

            end
        end
    end

    for _, unit in ipairs(GetEnemies()) do
        if ValidUnit(unit, "enemy") then
            noGroundAndReflect(unit)

            local spellName, _, _, _, _, endCast, _, _, protected = UnitCastingInfo(unit)
            local spellNames, _, _, _, start, _, _, protecteds, _ = UnitChannelInfo(unit)

            if spellName ~= nil and holder[spellName] ~= nil and
                    holder[spellName] > 0
                    and ((endCast/1000) - GetTime()) < lags + 10
                    and not protected then
                local level =  holder[spellName]
                if level == 3 then
                    catchGround(unit)
                end
            end

            if spellName ~= nil and holder[spellName] ~= nil and
                    holder[spellName] > 0
                    and ((endCast/1000) - GetTime()) < lags
                    and not protected then
                local level = holder[spellName]
                local yes = level == 3
                if ((level == 2 and EnnemiesAreUnder(80)) or level == 4) and ni.unit.los("player", unit) and CdRemains(57994) and IsSpellInRange(GetSpellInfo(57994), unit) == 1
                then
                    SpellStopCasting()
                    CastSpellA(57994, unit)
                else
                    catch(unit, yes)
                end

            elseif spellNames ~= nil and holder[spellNames] ~= nil and
                    holder[spellNames]
                    and EnnemiesAreUnder(80)
                    and not protecteds
                    and GetTime() - (start/1000) > 0.45 then
                if CdRemains(57994)
                        and IsSpellInRange(GetSpellInfo(57994), unit) == 1
                        and not HasAura(8178, "player") and ni.unit.los("player", unit) then
                    SpellStopCasting()
                    CastSpellA(57994, unit)
                end
            elseif HasAura(26889, unit)
                    or HasAura(5215, unit)
                    or HasAura(1784, unit)
                    or HasAura(58984, unit) then
                CastSpellA(49233, unit)

            elseif stopc == 0 then
                if (HasAura(48707, unit)
                        or HasAura(19263, unit)
                        or HasAura(45438, unit)) and UnitIsUnit("target", unit) and ni.unit.distance("player", unit) < 43 and ni.unit.los("player", unit) then
                    SpellStopCasting()
                    timerc = GetTime()
                    stopc = 1
                end
            end

            if (GetTime() - timerc) > 10 then
                stopc = 0
            end
        end
    end


    if not HasAura(57960, "player")
            and not HasAura(49281, "player")
            and not HasAura(49284, "player")
            and GetTime() - rebuff > 10
            and HealthTeamNotUnder(60) then
        CastSpellA(57960)
        rebuff = GetTime()
    elseif HasAura(2094, "player") then
        notice("UNDER BLIND", true)
    elseif HasAura(51724, "player") then
        notice("SAP", true)
    elseif HasAura(20066, "player") then
        notice("UNDER REPENTENCE", true)
    elseif HasAura(10308, "player") then
        notice("UNDER HOJ", true)
    end

    _, _, _, count, _, _, time, _, _, _, _ = UnitBuff("player", GetSpellInfo(53817))
    if count == 1 then
        if llastx ~= 1 then
            t1:SetTexture(0, 1.0, 0)
            t2:SetTexture(1.0, 0, 0)
            t3:SetTexture(1.0, 0, 0)
            t4:SetTexture(1.0, 0, 0)
            t5:SetTexture(1.0, 0, 0)
        end
        llastx = 1
    elseif count == 2 then
        if llastx ~= 2 then
            t1:SetTexture(0, 1.0, 0)
            t2:SetTexture(0, 1.0, 0)
            t3:SetTexture(1.0, 0, 0)
            t4:SetTexture(1.0, 0, 0)
            t5:SetTexture(1.0, 0, 0)
        end
        llastx = 2
    elseif count == 3 then
        if llastx ~= 3 then
            t1:SetTexture(0, 1.0, 0)
            t2:SetTexture(0, 1.0, 0)
            t3:SetTexture(0, 1.0, 0)
            t4:SetTexture(1.0, 0, 0)
            t5:SetTexture(1.0, 0, 0)
        end
        llastx = 3
    elseif count == 4 then
        if llastx ~= 4 then
            t1:SetTexture(0, 1.0, 0)
            t2:SetTexture(0, 1.0, 0)
            t3:SetTexture(0, 1.0, 0)
            t4:SetTexture(0, 1.0, 0)
            t5:SetTexture(1.0, 0, 0)
        end
        llastx = 4
    elseif count == 5 then
        if llastx ~= 5 then
            t1:SetTexture(0, 1.0, 0)
            t2:SetTexture(0, 1.0, 0)
            t3:SetTexture(0, 1.0, 0)
            t4:SetTexture(0, 1.0, 0)
            t5:SetTexture(0, 1.0, 0)
        end
        llastx = 5
        notice("INSTANT", false)
    elseif count == nil then
        if llastx ~= nil then
            t1:SetTexture(1.0, 0, 0)
            t2:SetTexture(1.0, 0, 0)
            t3:SetTexture(1.0, 0, 0)
            t4:SetTexture(1.0, 0, 0)
            t5:SetTexture(1.0, 0, 0)
        end
        llastx = nil
    end
end

if select(2, UnitClass("player")) == "SHAMAN" and  not okipop then okipop = true

    local timestamp = 0
    local framehandler = function(frame, elapsed)
        timestamp = timestamp + elapsed
        if timestamp > 0.1 then
            timestamp = 0
            myasd()
        end
    end

    local f = CreateFrame("frame")
    f:SetScript("OnUpdate", framehandler)
end

if not asdasd then
    asdasd = true
    PlaySound("AuctionWindowClose", "master")
else
    asdasd = false
    PlaySound("TalentScreenClose", "master")
end

local targetUnit = function(...) ni.functions.callprotected(TargetUnit, ...) end
local focusUnit = function(...) ni.functions.callprotected(FocusUnit, ...)  end
local clearFocus = function(...) ni.functions.callprotected(ClearFocus, ...)  end

if UnitName("player") == "Lockshamhero" then
    function addonHandle()
        local a1
        local a2
        local a3
        local target
        local focus

        a1 = UnitGUID("arena1")
        a2 = UnitGUID("arena2")
        a3 = UnitGUID("arena3")
        target = UnitGUID("target")
        focus = UnitGUID("focus")

        local healerUnit, healer = HealerToFocusIfAny()

        if target == nil then
            if focus ~= nil and focus == a1 and UnitExists("arena2") then
                targetUnit(a2)
            elseif focus ~= nil and focus == a1 and UnitExists("arena3") then
                targetUnit(a3)
            elseif focus ~= nil and focus == a2 and UnitExists("arena1") then
                targetUnit(a1)
            elseif focus ~= nil and focus == a2 and UnitExists("arena3") then
                targetUnit(a3)
            elseif focus ~= nil and focus == a3 and UnitExists("arena1") then
                targetUnit(a1)
            elseif focus ~= nil and focus == a3 and UnitExists("arena2") then
                targetUnit(a2)
            else
                if UnitExists("arena1") then
                    targetUnit(a1)
                elseif UnitExists("arena2") then
                    targetUnit(a2)
                elseif UnitExists("arena3") then
                    targetUnit(a3)
                end
            end
        elseif healer ~= nil and target ~= healer and UnitExists(healerUnit) then
            focusUnit(healerUnit)
        elseif target == a1 and focus == nil then
            if UnitExists("arena2") then
                focusUnit(a2)
            elseif UnitExists("arena3") then
                focusUnit(a3)
            end
        elseif target == a2 and focus == nil then
            if UnitExists("arena1") then
                focusUnit(a1)
            elseif UnitExists("arena3") then
                focusUnit(a3)
            end
        elseif target == a3 and focus == nil then
            if UnitExists("arena1") then
                focusUnit(a1)
            elseif UnitExists("arena2") then
                focusUnit(a2)
            end
        end

        if target ~= nil and target == focus then
            clearFocus()
        end
    end


    local timestamp = 0
    local framehandler = function(frame, elapsed)
        timestamp = timestamp + elapsed
        if timestamp > 0.1 then
            timestamp = 0
            if not IsActiveBattlefieldArena() then return end

            if UnitExists("arena3") then
                addonHandle()
                return
            end

            local a1
            local a2
            local target
            local focus

            a1 = UnitGUID("arena1")
            a2 = UnitGUID("arena2")
            target = UnitGUID("target")
            focus = UnitGUID("focus")

            if target == nil then
                if focus ~= nil and focus == a1 and UnitExists(a2) then
                    targetUnit(a2)
                else
                    if UnitExists(a1) then
                        targetUnit(a1)
                    elseif UnitExists(a2) then
                        targetUnit(a2)
                    end
                end
            elseif target == a1 and UnitExists(a2) and focus ~= a2 then
                focusUnit(a2)
            elseif target == a2 and UnitExists(a1) and focus ~= a1 then
                focusUnit(a1)
            end

            if target ~= nil and target == focus then
                clearFocus()
            end
        end
    end

    local f = CreateFrame("frame")
    f:SetScript("OnUpdate", framehandler)
end
